from django.urls import path
from . import views
from django.contrib.auth import views as auth
  
urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.user_login, name='login'),
    path('logout', auth.LogoutView.as_view(template_name ='index.html'), name='logout'),
    path('sign_up', views.sign_up, name = 'sign_up'),
    path('reset', views.ResetPasswordView.as_view(), name='reset'),
    path('reset-confirm/<uidb64>/<token>/',
         auth.PasswordResetConfirmView.as_view(template_name='reset_confirm.html'),
         name='reset_confirm'),
    path('password_reset-complete/',
         auth.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'),
         name='password_reset_complete'),
]
